FROM squidfunk/mkdocs-material


COPY requirements.txt .

RUN apk add --no-cache gcc musl-dev

RUN pip install -r requirements.txt

