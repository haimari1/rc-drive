---
title: Join us at OMER RING Track 🏎️
disqus: ''
---

# Join Us 🏎️

!!! summary "Join us"

    ![Image title](/img/IMG_0530.webp){ align=right width="512" }

    If you have the itch to drive your own RC 1/8 scale buggy, join us.
    
    Meet some of the best Off Road drivers, current and past champions of Nitro and Electric RC cars.

!!! tip "Contact us"

    Asi: +972-54-800-8456

  






