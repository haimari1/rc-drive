---
title: Omer Ring Off Road Track 🏁
disqus: ''
---

# Omer Ring - Off Road Track 🏁

<!-- 
![](/img/logo.webp){:height="250px" width="250px"} -->

!!! summary "עומר רינג מסלול מרוצים למכוניות שלט" 

    ![Image title](/img/Gal-Yaniv.webp){ align=right width="512" }


    Located in Eyn Vered (Near Tel Mond) this track is specifically designed for 1/8 Scale Buggy. 
    🤍 (and now Truggy too)

    When becoming a member you will get full access (almost 24/7) to come and train with fellow drivers.

    Improve your driving skills, and get the best tips from experienced drivers.

!!! summary "2.12.2022 - Unforgettable Day at Omer Ring Track"

    <iframe width="1024" height="512" src="https://www.youtube.com/embed/IaLFAb-LBms" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

!!! note "Just having some fun on the track"

    <iframe width="1024" height="512" src="https://www.youtube.com/embed/JkPHibzxTbU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


!!! tip "Jump right in"

    Join the league, beat the clock and maybe you will become a champion 🏆
    In anyway, it's a lot of fun !
 

!!! question "Want to join us ? רוצים להצטרף אלינו"

    [click Here](join-us.md#)

    Anyone can join and it's never too late or early. 
    
    Did you know that the current 🇮🇱 Champion  is Harel Senderov 🏆 and he is not even 15 years old !

    ![Image title](/img/podium.webp){ align=left width="1024" }


![Taipei, Taiwan. Credit: Yuyu Liu](/img/jump.jpg){data-gallery="taipei"}

![Taipei, Taiwan. Credit: Yuyu Liu](/img/pit.jpg){data-gallery="taipei"}
    
[Join Us :fontawesome-solid-paper-plane:](/join-us){ .md-button .md-button--primary}


    







