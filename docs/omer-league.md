---
title: Omer League 2022-2023 🇮🇱
disqus: ''
---

## 🏁 16.12.2022 🏆

[Race Event - 16.12.2022](https://israelileague.liverc.com/results/?p=view_event&id=393491){ target=blank }


!!! summary 

    The 🌧️ from the days before and a muddy track did not stop anyone from giving their best fight

    After some laps in the mud, the ☀️ did it's thing and the track got less wet, more sandy and hot.

    The facility itself was recently upgraded, making it a fabulous place to race!

    After 4 ranking divisions in the 1:8 nitro category:
    
     *Yaniv Sivan*🥇 was the fastest driver in the Nitro category, and *Yoni Kozak*🥇 in the 1:8 electric category.

    !!! tip "Nitro Final A 🏁 " 

        Final A - 35 minutes long opened with *Yaniv Sivan* taking the lead in the first laps. 
        
        Gil Harush competed with *Yaniv* for the first for several laps but choosing the wrong tires caused him to lose valuable time. 
        
        *Amit Bublil* took the second, towards the end of the final *Gal Kinan* and *Gil Harush* fought for The third.
        
        *Gal* received a motor cut, *Gil* took the 3rd. 
        
        At the end, *Yaniv Sivan* won the race, 
        2nd was *Amit Bublil*, 3rd was *Gil Harush*.


    !!! tip "Nitro Final B 🏁"

        Final B In the 30-minute final, *Guy Ben Harosh* started strong and 🔥 the track, 2nd was *Ronen Alkalai* and 3rd *Eran Tal*. 
        
        *Hovav* had an engine problem right from the launch, he quickly replaced the engine and returned to the action.
        
        In the meantime, Eran had cuts and lost the third place to Yair Tilo.
        
        At the end, Guy Ben Harosh won the race, 2nd Ronen Alkalai, 3rd Yair Tilo.

        It was a great day on the track, congratulations to the winners !
           
    Author: [Yaniv Sivan](https://www.facebook.com/1375002847/posts/pfbid08PibopR4WWXUtWYqEG5Aaf2UmiLRb3rKhNqRJrQLDoRu3tGUbv4yra257opSkoixl/?sfnsn=mo&mibextid=VhDh1V){ target=blank }

!!! summary "A Main Nitro 1:8"

    ![Image title](/img/IMG_1015.webp){ align=right width="512" }

    * 1st Yaniv Sivan 🥇
    * 2nd Amit Bublil 🥈
    * 3rd Gil Harush 🥉

!!! example "B Main Nitro 1:8"

    ![Image title](/img/IMG_0999.webp){ align=right width="512" }

    * 1st Guy Ben Harush 🥇
    * 2nd Ronen Alkalay 🥈 (Hovav in the pic)
    * 3rd Yair Tilo 🥉

!!! tip "Electric 1:8"

    ![Image title](/img/IMG_0992.webp){ align=right width="512" }

    * 1st Yoni Kozak 🥇
    * 2nd Maor Navon 🥈
    * 3rd Eyal Kozak 🥉


## 🏁 27.01.2023 🏆

[Race Event - 27.01.2023](https://israelileague.liverc.com/results/?p=view_event&id=396520){ target=blank }


!!! summary

    Race #2 took place at the Omer Ring Track in Ein Vered,
    It was an amazing day, with perfect weather, lots of dramas and action.

    The *Truggy* category joined for the first time and showed what they can do on the track.

    Special thanks to *Zachi Keynan* who run the race like a true professional.

    After three ranking divisions in Nitro Buggy that the best 2/3 are taken, *Harel Senderov* and *Yaniv Sivan* were the same in scoring and a tie breaker in times established that *Harel Senderov* TQ and also in the Electric Buggy category. 
    
    In the Truggy category *Guy Almog*🥇 was TQ.

    !!! tip "Nitro Final A 🏁 " 

        The A Nitro final heat started with *Harel Senderov* storming a head and managed to escape to the rest of the group. 
        In the meantime, *Yaniv Sivan* took 2nd safely, *Amit Bublil* and *Boris Melnik* competed on the 3rd, *Amit* got an engine cut and lost precious time, meanwhile *Gil Harush* who made an amazing comeback from the end, competed with *Boris Melnik* for the 3rd, *Boris* got a cut-off and *Gil Harush* by fast driving took the 3rd. 
        
        The dramas did not end as three minutes to the end *Harel Senderov* got a cut-off, *Yaniv Sivan* took the lead but a mistake by Yaniv at the end of the straight line caused him to lose the lead. 
        
        In the 1st place finished *Harel Senderov*🥇, 2nd *Yaniv Sivan*🥈, 3rd *Gil Harush*🥉 one of the craziest races ever!.

    !!! tip "Nitro Final B 🏁 " 

        Final in Nitro Buggy opened with *Gal Kenan* starting the race strong, 2nd *Ronen Alkalai* and 3rd *Ziv Magnazi*, Gal had a bad landing and broke suspension arm, but with a sporting spirit he quickly fixed it and returned to the track, RESPECT!
        
        In the meantime, *Ronan Alkalai* took the lead, 2nd by *Ziv Magnazi*. *Ronan Alkalai* got an engine failure and Ziv took first place even though he had an engine failure but managed to keep the lead and finish 1st, *Ronan Elkalai* 2nd and Gal Keinan 3rd.

    !!! tip "Electric Buggy 🏁 "

        In the electrical category Buggy *Harel Senderov*🥇 wins the first two finals and reserves the 1st place, 2nd is *Yoni Kozak*🥈 and 3rd *third Maor Navon*🥉.

    !!! tip "Electric Truggy 🏁 "


        In the new electric Truggy category with spectacular driving, the winner is *Guy Almog*🥇 , 2nd *Beni Briga*🥈, 3rd *Yoni Bitan*🥉. 

        It was fun to see the Truggy vehicles in action again after more than a decade and this time in the electric format.

    In conclusion, it was a crazy race, a fun racing day, a superlative experience and atmosphere in a top quality racing facility.
    Warm up your engines, race #3 will take place on 24.2.23.

    Author: [Yaniv Sivan](https://www.facebook.com/1375002847/posts/pfbid02uwUmzKS8dCqRUqXCxW3vRS2EB1DRF4VZtVmqXbgDVcaf3k9tHN2wRRZTDLumGxgl/?mibextid=Nif5oz){ target=blank }

!!! summary "A Main Nitro 1:8"

    ![Image title](/img/27.01.2023/Nitro-A.webp){ align=right width="512" }

    * 1st Harel Senderov 🥇
    * 2nd Yaniv Sivan 🥈
    * 3rd Gil Harush 🥉

!!! summary "B Main Nitro 1:8"

    ![Image title](/img/27.01.2023/Nitro-B.webp){ align=right width="512" }

    * 1st Ziv Magnezi 🥇
    * 2nd Ronen Alkalay 🥈
    * 3rd Gal Kenan 🥉
    
!!! summary "Electric 1:8"

    ![Image title](/img/27.01.2023/Electric.webp){ align=right width="512" }

    * 1st Harel Senderov 🥇
    * 2nd Yoni Kozak 🥈
    * 3rd Maor Navon 🥉

!!! summary "Truggy"

    ![Image title](/img/27.01.2023/Truggy.webp){ align=right width="512" }

    * 1st Guy Almog 🥇
    * 2nd Benny Briga 🥈
    * 3rd Yoni Bitan 🥉



## 🏁 24.02.2023 🏆

[Race Event - 24.02.2023](https://israelileague.liverc.com/results/?p=view_event&id=398882){ target=blank }


!!! summary

    On Friday, February 24, 2023, race #3 was held for 1:8 Buggy Nitro/Electric, and Electric Truggy in the race track Omer Ring (Ein Vered) after the windy weather we received perfect weather for races.
    Special thanks to the race judge *Tzahi Kenan* who perfectly managed the race from start to finish.
    Thanks to everyone prepared the track which was in great shape and fun and drive on. 
    
    It was an unusual racing experience !!!

    After four ranking rounds:

    In the 1:8 Buggy Nitro + Electric category it was *Harel Senderov*🥇 which was the fastest driver.

    In the 1:8 electric Truggy category, *Yoni Bitan*🥇 was the fastest driver.

    Nitro Finals opened with *Harel Senderov* and *Yaniv Sivan* were tight but Yaniv made a mistake and lost valuable time while Boris Melnick jumped to the second place but a cut-off made him lose valuable time as well, Gil Harush and Gal Kenan competed on the 3rd, Gil managed to quickly catch the 3rd. 
    
    In the meantime, Yaniv Sivan made a comeback to 2nd place. Minutes before the race was completed, Gil Harush got a motor cut-off and returned to the 4th place, Gal Kenan grabbed the 3rd. 
    
    On the last lap, Harel, who led the race safely, received a motor cut-off and yet kept his place. 
    At the end of *Harel Senderov*  finishes 1st, 2nd was *Yaniv Sivan* 🥈, and 3rd Gal Kenan  !!!

    In the electric buggy category, *Harel Senderov*🥇 wins two finals and takes 1st places, 2nd *Yoni Kozac*🥇, 3rd *Maor Navon*🥉 .

    In the truggy category, 1st *Yoni Bitten*🥇 2nd *Guy Almog* 🥈 3rd *Beny Briga*🥉.

    Author: [Yaniv Sivan](https://m.facebook.com/story.php?story_fbid=pfbid0w3Kz17RqLccykY4c2g7CACjTPEWNGNNpZwtHjEs1iLQbHPWdnWqataz5AbNNTgGal&id=1375002847&mibextid=Nif5oz){ target=blank }


!!! summary "A Main Nitro 1:8"

    ![Image title](/img/24.02.2023/Nitro-A.webp){ align=right width="512" }

    * 1st Harel Senderov 🥇
    * 2nd Yaniv Sivan 🥈
    * 3rd Gal Kenan 🥉

!!! summary "B Main Nitro 1:8"

    ![Image title](/img/24.02.2023/Nitro-B.webp){ align=right width="512" }

    * 1st Ronen Alkalai 🥇
    * 2nd Ziv Magnezi 🥈
    * 3rd Yehuda Moyal 🥉

!!! summary "Electric 1:8"

    ![Image title](/img/24.02.2023/Electric.webp){ align=right width="512" }

    * 1st Harel Senderov 🥇
    * 2nd Yoni Kozak 🥈
    * 3rd Maor Navon 🥉

!!! summary "Truggy"

    ![Image title](/img/24.02.2023/Truggy.webp){ align=right width="512" }

    * 1st Yoni Bitan 🥇
    * 2nd Guy Almog 🥈
    * 3rd Beny Briga 🥉


## 🏁 17.03.2023 🏆

[Race Event - 17.03.2023](https://israelileague.liverc.com/results/?p=view_event&id=400701){ target=blank }

!!! summary

    ![Image title](/img/pit.webp){ align=center width="512" }

    On Friday 17.03.2023 race number 4 for 1/8 nitro/electric buggies took place at the Ein Vared race track.
    The weather was perfect and we won a day of racing full of action dramas and adrenaline.

    ![Image title](/img/jump.webp){ align=center width="512" }

    After 4 classification rounds Harel Senderov was the fastest driver in both nitro and electric categories.
    Final A Nitro opened with a drama as Harel Senderov encountered a mechanical problem and did not start the heat, Yaniv Sivan took the lead and caught a gap, meanwhile Boris Melnik was second, Gal Kenan and Ziv Magenzi who advanced from Final B started the race well and competed for the 3rd place.
    
    ![Image title](/img/drive.webp){ align=center width="512" }

    Ziv lost valuable time due to an accident and with an engine problem, Harel Senderov fixed his car but problems with the engine caused him to lose more time. At the end he finished the race with a sense of sportsmanship.

    At the end, *Yaniv Sivan* 🥇 was 1st, *Boris Melnik* 🥈 was 2nd and *Gal Kenan* 🥉 was 3rd.

    The Nitro final opened with a lot of drama in the first laps, Gal Kenan started first, followed by Ziv Magenzi and Ronan Alkalai in 3rd, an electrical issue in Gal's car caused him to lose valuable time, but despite this he came back, in the meantime Ziv Magenzi led the race, followed by Ronen Alkalai who suffered from engine problems, Yehuda Moyal drove steadily and took the third place, Gal who came back from the crash closed on Ziv, Ziv's cut-off allowed him to take the lead.


    At the end *Gal Kenan* 🥇 1st, *Ziv Magenzi* 🥈 2nd, *Yehuda Moyal* 🥉 3rd.

    In the electrical category, Harel Senderov dominated the first two finals and finished 1st.
    Maor Navon and Yoni Kozak competed for 2nd place and the difference between them was one point in favor of Yoni after three 8 minute finals.

    Harel Senderov 🥇 1st, Yoni Kozak 🥈 2nd, Ma'or Navon 🥉 3rd.
  
    Author: [Yaniv Sivan](https://www.facebook.com/groups/4232892586762714/permalink/6362464303805521/?mibextid=Nif5oz){ target=blank }


!!! summary "A Main Nitro 1:8"

    ![Image title](/img/17.03.2023/Nitro-A.webp){ align=right width="512" }

    * 1st Yaniv Sivan 🥇
    * 2nd Harel Senderov 🥈
    * 3rd Gal Kenan 🥉

!!! summary "B Main Nitro 1:8"

    ![Image title](/img/17.03.2023/Nitro-B.webp){ align=right width="512" }

    * 1st Gal Kenan 🥇
    * 2nd Ziv Magnezi 🥈
    * 3rd Yehuda Moyal 🥉

!!! summary "Electric 1:8"

    ![Image title](/img/17.03.2023/Electric.webp){ align=right width="512" }

    * 1st Harel Senderov 🥇
    * 2nd Yoni Kozak 🥈
    * 3rd Maor Navon 🥉




## 🏁 21.04.2023 🏆

[Race Event - 21.04.2023](https://israelileague.liverc.com/results/?p=view_event&id=403646){ target=blank }

!!! summary

    ![Image title](/img/21.04.2023/kyosho-team-21-04-2023.webp){ align=center width="512" }

    Race number 5 in the Omer League at the Omer Ring track for 1/8 nitro and electric buggies started last Friday. 
    A considerable amount of drivers came and competed in one of the crazy races held in the league.
    After four ranking divisions in the 1/8 buggy category Liron Beiser was the fastest driver (TQ)


    ![Image title](/img/21.04.2023/DSC01638.jpg.webp){ align=center width="512" }

    In the 1/8 electric buggy category it was Harel Senderov (TQ)

    ![Image title](/img/21.04.2023/DSC01619.jpg.webp){ align=center width="384" }


    Final A Nitro Buggy started with Liron Beiser and Harel Senderov starting strong, meanwhile Yaniv Sivan from sixth place very quickly took third place and closed the gap on the two, 
    
    ![Image title](/img/21.04.2023/DSC01539.jpg.webp){ align=center width="512" }

    after 5 minutes Liron with engine problems lost valuable time and moved to third place. 
    Yaniv continued to close the gap with the fastest lap time in the race 40.952 on Harel, the distance between them reached up to 6 seconds, but Harel kept his cool and drove at a fast pace without making a mistake. 
    
    ![Image title](/img/21.04.2023/DSC01413.jpg.webp){ align=center width="512" }

    
    In the meantime, Amit Bobalil managed to take the third place. 
    
    The drama over the third place reached its peak in the last lap when the difference between Amit and Liron was less than a second, Amit's rollover in the long jump allowed Liron to overtake. 

    Winners: 1st Harel Senderov 🥇, second Yaniv Sivan 🥈, third Liron Beiser 🥉.

    Nitro B Final:
    One of the crazy finals that was 30 minutes of upheavals. It started when Ronen Alkalai driving fast led the race safely, second Guy Ben Arush, third Ziv Magenzi. 
    
    Seven minutes into the race Ronan and Guy got an engine cut and lost their position, Ziv took the lead. In the meantime, Gal Keinan took second place and towards the middle of the race he took the lead, but he also encountered a problem with his car which caused him to return to seventh place. 
    
    In the meantime Gil Harush who didn't start the race so well, was driving fast and made an amazing comeback from the last place to the second place but towards the end of the race he got an engine failure. 
    
    Guy Ben Arush, who drove fast and safely maintained second place, competed with Ronen Alkalai, who made a comeback to advance to the A final, but an engine failure by both of them allowed Asi Biton, who bumped up from C final, to take second place and advance to final A.

    On the podium - 1st Ziv Magenzi 🥇 with fantastic driving, 2nd was Asi Biton 🥈, Guy Ben Arush 3rd 🥉.

    Final C Nitro Buggy

    Asi Biton 🥇 , who started first, took the lead and jumped to the A final safely by finishing 1st. 
    Daniel Kugler 🥈 in 2nd place kept his place throughout the race and took 2nd place in the race.
    Yehuda Moyal and Hovab Siboni (Red Bull Team) competed in the 3rd place by the end, Hovav 🥉 had the upper hand finishing on 3rd.

    Category 1/8 electrical buggies
    
    Electric A Main:
    Harel Senderov won 2 out of 3 races, promesing the 1st place. 
    Yoni Kozak and Maor Navon competed for the 2nd place, they were close, until the last final where Yoni had a bad start to the heat, meanwhile Maor took the lead Yoni drove fast and closed on Maor and in the last lap the two collided on the far ramp (had a racing accident)

    And at the end: Yoni Kojak 🥇  wins the third final, 2nd Maor Navon 🥈 3rd Haim Ari 🥉.

    In the summary of the three finals: 1st Harel Senderov🥇 2nd Yoni Kozak🥈 3rd Maor Navon🥉.

    Electric B Main:

    Guy Almog🥇 wins two finals and is first overall, Benny Briga🥈 finishes in 2nd place and closes the podium with Eran Kave🥉 in 3rd place.

    In conclusion
    It was an outstanding race, with great atmosphere and an unusual driving experience!

    Author: [Yaniv Sivan](https://m.facebook.com/story.php?story_fbid=pfbid08i8puzcaCsXhEUxf5oped69DV5fVUjqDYNhxb8gLn4oZgewFwG4HpNwqHb7P2La5l&id=1375002847&mibextid=Nif5oz){ target=blank }

    Photos: Benny Briga

!!! summary "A Main Nitro 1:8"

    ![Image title](/img/21.04.2023/Nitro-A.webp){ align=right width="512" }

    * 1st Harel Senderov 🥇
    * 2nd Yaniv Sivan 🥈
    * 3rd Liron Beiser🥉

!!! summary "B Main Nitro 1:8"

    ![Image title](/img/21.04.2023/Nitro-B.webp){ align=right width="512" }

    * 1st Ziv Magnezi 🥇
    * 2nd Asi Biton 🥈
    * 3rd Guy Ben Arush 🥉

!!! summary "C Main Nitro 1:8"

    ![Image title](/img/21.04.2023/Nitro-C.webp){ align=right width="512" }

    * 1st Asi 🥇
    * 2nd Daniel Kugler 🥈 (Yair Tilo at the pic)
    * 3rd Hovav 🥉

!!! summary "A Main Electric 1:8"

    ![Image title](/img/21.04.2023/Electric-A.webp){ align=right width="512" }

    * 1st Harel Senderov 🥇
    * 2nd Yoni Kozak 🥈
    * 3rd Maor Navon 🥉

!!! summary "B Main Electric 1:8"

    ![Image title](/img/21.04.2023/Electric-B.webp){ align=right width="512" }

    * 1st Guy Almog 🥇
    * 2nd Benny Briga 🥈
    * 3rd Eran Kave 🥉


## 🏁 19.05.2023 🏆

[Race Event - 19.05.2023](https://israelileague.liverc.com/results/?p=view_event&id=405902){ target=blank }

!!! summary

    ![Image title](/img/19.05.2023/1.webp){ align=center width="512" }

    Race number 6 in memory of Onmer London took place at the Omer Ring track for 1/8 nitro, 1/8 electric and also electric Truggy started on Friday. 
    It was a huge event, with many racers and visitors on the track.



![Image title](/img/19.05.2023/IMG_1606.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1612.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1616.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1620.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1621.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1622.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1636.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1644.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1671.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1677.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1678.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1680.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1683.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1685.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1686.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1690.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1691.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1696.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1710.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1711.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1713.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1715.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1718.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1735.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1739.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1740.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1741.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1744.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1745.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1748.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1751.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1752.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1757.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1760.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1765.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1766.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1771.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1772.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1773.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1776.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1784.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1788.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1790.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1794.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1795.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1796.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1797.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1799.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1800.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1801.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1802.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1803.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1804.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1808.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1809.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1812.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1814.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1825.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1837.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1838.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1839.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1840.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1845.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1849.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1858.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1860.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1861.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1862.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1872.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1874.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1881.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1884.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1890.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1892.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1906.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1916.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1917.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1918.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1919.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1922.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1924.webp){ align=center width=1024 }

![Image title](/img/19.05.2023/IMG_1925.webp){ align=center width=1024 }


!!! summary "A Main Nitro 1:8"

    * 1st Liron Bizer 🥇
    * 2nd Yaniv Sivan 🥈
    * 3rd Ido Carmon🥉

!!! summary "B Main Nitro 1:8"

    * 1st Boris Melnik 🥇
    * 2nd Asi Biton 🥈
    * 3rd Guy Ben Arush 🥉

!!! summary "C Main Nitro 1:8"

    * 1st Ziv Magenzi 🥇
    * 2nd Shachar Marziano 🥈 
    * 3rd Eran Tal 🥉

!!! summary "A Main Electric 1:8"

    ![Image title](/img/19.05.2023/4.webp){ align=right width="512" }

    * 1st Harel Senderov 🥇
    * 2nd Amit Bublil 🥈
    * 3rd Maor Navon 🥉

!!! summary "B Main Electric 1:8"

    ![Image title](/img/19.05.2023/2.webp){ align=right width="512" }

    * 1st Yehuda Moyal 🥇
    * 2nd Guy Almog 🥈
    * 3rd TZvik 🥉

!!! summary "A Main Electric Truggy"

    * 1st Amit Bublil 🥇
    * 2nd Maor Navon 🥈
    * 3rd Guy Almog 🥉

    Photos: Haim Ari

    Author: [Yaniv Sivan](https://m.facebook.com/story.php?story_fbid=pfbid02FmoC1SkfKEaKMWrLW57f4iVYWhtVC9e6f2bx9VDjeTVC6SyUHTvBKNKL4RaYeCNul&id=1375002847&sfnsn=mo&mibextid=RUbZ1f){ target=blank }


## 🏁 16.06.2023 🏆 Finals

[Race Event - 16.06.2023](https://israelileague.liverc.com/results/?p=view_event&id=405902){ target=blank }

!!! summary


    
    Yaniv Sivan is the 2023 Israeli 1:8 Nitro Buggy Champion. 
    
    The title being decided at the final round of the seven championship at Omer Ring racing track near Tel-Aviv.  After 6 races the situation in the championship was that Sivan was leading the championship by 4 points, Harel Senderov in second had to win the seventh race to win the championship.  
    
    The 35 min A-final started while Yaniv Sivan (TQ) with his Kyosho MP10 TKI3 opened a 15 secs gap on Second place Harel Senderov with his Mugen MBX8R which had early battle with Ido Carmon on second spot.  However, when Yaniv’s car exit the first pit stop another car was lowered in the same time and crashed at him, Yaniv lost valuable time and the gap went down to 5 secs. 
    
    Nevertheless, Yaniv managed to keep the gap safe till 15 min to the end of the race Yaniv Sivan made a mistake at far downhill and Harel passed him.  The two drivers battled for the first back and forth till one and half minutes to the end Harel Senderov crashed at the end of the straight and Yaniv Sivan passed him but still the gap between them was less than a second. 
    
    On the last Yaniv Sivan managed to hold the first position and passed the start finish line first and become the 2023 Israeli 1:8 Nitro Buggy Champion.  In  eBuggy Senderov took the title from Round 7 winner Yoni Cozac.

    <iframe width="1024" height="512" src="https://www.youtube.com/embed/vJi_HqE8LPk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

    ![Image title](/img/16.06.2023/20230616_174629.webp){ align=center width=1024 }

    ![Image title](/img/16.06.2023/20230616_174724.webp){ align=center width=1024 }

    ![Image title](/img/16.06.2023/20230616_174753.webp){ align=center width=1024 }

    ![Image title](/img/16.06.2023/20230616_174808.webp){ align=center width=1024 }

    ![Image title](/img/16.06.2023/20230616_174820.webp){ align=center width=1024 }

    ![Image title](/img/16.06.2023/20230616_174840.webp){ align=center width=1024 }

    ![Image title](/img/16.06.2023/20230616_174855.webp){ align=center width=1024 }

    ![Image title](/img/16.06.2023/20230616_174911.webp){ align=center width=1024 }

    ![Image title](/img/16.06.2023/20230616_175103.webp){ align=center width=1024 }
    
    ![Image title](/img/16.06.2023/20230616_175113.webp){ align=center width=1024 }

    ![Image title](/img/16.06.2023/20230616_175228.webp){ align=center width=1024 }

    ![Image title](/img/16.06.2023/20230616_175249.webp){ align=center width=1024 }

    ![Image title](/img/16.06.2023/20230616_175306.webp){ align=center width=1024 }

    ![Image title](/img/16.06.2023/20230616_175311.webp){ align=center width=1024 }

    ![Image title](/img/16.06.2023/20230616_175446.webp){ align=center width=1024 }

    ![Image title](/img/16.06.2023/20230616_175504.webp){ align=center width=1024 }

    ![Image title](/img/16.06.2023/20230616_175514.webp){ align=center width=1024 }

    ![Image title](/img/16.06.2023/20230616_175616.webp){ align=center width=1024 }



!!! info
    
    Nitro 1:8 Buggy Championship results top 8 drivers (5 best from 7 races count)

    1. Yaniv Sivan – 496 Kyosho/Reds/Hot Race 🏆
    2. Harel Senderov – 495 Mugen/Alpha/Jconcept 🏆
    3. Boris Melnik – 479 Associated/Alpha/Jconcept 🏆
    4. Amit Bublil – 478 Associated/Alpha/Jconcept 🏅
    5. Gal Kenan – 474 Mugen/Alpha/Jconcept 🏅
    6. Gil Harush – 471 Kyosho/OS/Pro-Line 🏅
    7. Ziv Magnezi – 467 Kyosho/Reds/6MIK 🏅
    8. Guy Ben Arush – 466 Kyosho/OS/6MIK 🏅

    Race #7 Top 3 Nitro

    1. Yaniv Sivan 🥇
    2. Harel Senderov 🥈
    3. Ido Carmon 🥉

    Electric 1:8 buggy Championship results top 5 drivers 

    1. Harel Senderov – 500 Mugen 🏆
    2. Yoni Cozac – 494 Mugen 🏆
    3. Maor Navon – 488 Kyosho 🏆
    4. Haim Ari – 475 Kyosho 🏅
    5. Guy Almog – 475 TLR 🏅

    Race #7 Top 3 Electric

    1. Yoni Cozac 🥇
    2. Maor Navon 🥈
    3. Guy Almog 🥉
   
    Source: [Redrc.net](https://www.redrc.net/2023/06/sivan-crowned-israeli-18-nitro-buggy-champion-report/){ target=blank }

    Video & Photos: Yaniv Sivan, RC-Zone

    Author: [Yaniv Sivan](https://m.facebook.com/story.php?story_fbid=pfbid02ALQyZqkGFLAvxR7SSJX1eqJuhfNwt4cpPuUUAGJe8a8NK4o2X2GseFZi9aAjDF4Ml&id=1375002847&sfnsn=mo&mibextid=RUbZ1f){ target=blank }


[Join Us :fontawesome-solid-paper-plane:](/join-us){ .md-button .md-button--primary}