---
title: Building RC Kit 🪛
disqus: ''
---

# Building RC Kit 🪛


<!-- 
![](/img/logo.webp){:height="250px" width="250px"} -->

!!! warning "Building the Kyosho MP10E"

    ![Image title](/img/IMG_0530.webp){ align=right width="512" }

    Building only **looks** complicated, but before you know it you will get from this: 


!!! note "to this:"

    ![Image title](/img/IMG_0544.webp){ align=right width="512" }


!!! success "to this"

    ![Image title](/img/IMG_0577.webp){ align=right width="512" }

    <!-- See some more pictures of the build process [Here](https://www.icloud.com/sharedalbum/#B175qXGF1r2ZyEG) -->


[Join Us :fontawesome-solid-paper-plane:](/join-us){ .md-button .md-button--primary}
